<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index(){
        $datas=DB::table('casts')->get();
        return view('cast.index',compact('datas'));
    }
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request){
        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
       
return redirect('cast');

    }
    public function show($id){
        $cast = DB::table('casts')->where('id', $id)->first();
      //  dd($post);
        return view('cast.edit', compact('cast'));

    }
    public function update($id ,Request $request){
        $query = DB::table('casts')
        ->where('id', $id)
        ->update([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"],
        ]);
    return redirect('/cast');

    }
    
public function destroy($id)
{
    $query = DB::table('casts')->where('id', $id)->delete();
    return redirect('/cast');
}
}
