<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', function () {
    return view('layout.master');
});
Route::get('/tabel', function () {
    return view('tabel');
});
Route::get('/data-tabel', function () {
    return view('data-tabel');
});
Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast/store', 'CastController@store');
Route::get('/cast/{id}', 'CastController@show');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@destroy');
