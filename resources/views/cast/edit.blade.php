@extends('layout.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form method="POST" action="/cast/{{ $cast->id }}">
        @csrf
        @method("PUT");
        <input type="hidden" name="id" value="{{ $cast->id }}">
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}">

            </div>
            <div class="form-group col-3">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{ $cast->umur }}">

            </div>

            <div class="form-group">
                <label>Biografi</label>
                <textarea class="form-control" rows="3" name="bio" value="">{{ $cast->bio }}</textarea>
            </div>

        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection
