@extends('layout.master')
@section('judul')
    Tambah Data
@endsection
@section('content')
    <form method="POST" action="store">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">

            </div>
            <div class="form-group col-3">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan umur">

            </div>

            <div class="form-group">
                <label>Biografi</label>
                <textarea class="form-control" rows="3" name="bio" placeholder="islah"></textarea>
            </div>

        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection
