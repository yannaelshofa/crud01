@extends('layout.master');
@section('judul')
    Data Peran
@endsection
@section('content')
    <a href="/cast/create">Tambah Data</a>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($datas as $item => $values)
                <tr>
                    <td>{{ $values->id }}</td>
                    <td>{{ $values->nama }}</td>
                    <td>{{ $values->umur }}</td>
                    <td>{{ $values->bio }}</td>
                    <td><a href="/cast/{{ $values->id }}">Edit</a>
                        <form action="/cast/{{ $values->id }}" method="post">
                            @csrf
                            @method("DELETE")
                            <input type="submit" value="Hapus">
                        </form>
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
