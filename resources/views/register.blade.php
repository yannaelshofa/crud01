@extends('layout.master')
@section('judul')
    Buat Account
@endsection
@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <H1>Buat Account Baru</H1>
        <h3>Sign Up Form</h3>
        First Name
        <br>
        <input type="text" name="nama" id="">
        <br>
        Last Name:
        <br>
        <input type="text" name="last" id="">
        <br>
        Gender
        <br>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-primary">
                <input type="radio"> Male
            </label>
            <br>
            <label class="btn btn-primary">
                <input type="radio"> Female
            </label>
            <br>
            <label class="btn btn-primary">
                <input type="radio"> secreat
            </label>
        </div>
        <br>
        Nationaly
        <select name="negara" id="">
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
        </select>
        <br>
        Langguage Spoken
        <div class="btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-primary">
                <input type="checkbox"> Indonesia
            </label>
            <br>
            <label class="btn btn-primary">
                <input type="checkbox"> Engglish
            </label>
            <br>
            <label class="btn btn-primary">
                <input type="checkbox"> Other
            </label>
        </div>
        bio:
        <br>
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="SignUp">
    </form>
@endsection
